# #MTG Card Finder

<br>

## Find Your Favorite Magic The Gathering Card!

An Android Kotlin app to search MTG cards.

<br>

### Features:

* Search cards by name or type;
* Save them for future use;
* See the card's image and associated text.

<br>

#### Examples:

<img src="screenshots/screenshots1.jpg" height="300" width="150"> <img src="screenshots/screenshots2.jpg" height="300" width="150"> <img src="screenshots/screenshots3.jpg" height="300" width="150">
<br>
<img src="screenshots/screenshots5.jpg" height="300" width="150"> <img src="screenshots/screenshots4.jpg" height="300" width="150"> <img src="screenshots/screenshots7.jpg" height="300" width="150">
<br>
<img src="screenshots/screenshots9.jpg" height="300" width="150"> <img src="screenshots/screenshots8.jpg" height="300" width="150"> <img src="screenshots/screenshots6.jpg" height="300" width="150">
<br>

##### This was done to better learn Google's "Android Kotlin Fundamental Codelabs", sections 9 and 10, and Google's "Android: Material Advanced Components (Kotlin) Codelabs"
