package eduardoaemunoz.mtgcardfinder.database


import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized


@Database(entities = [CardEntity::class], version = 1, exportSchema = true)
abstract class CardDatabase : RoomDatabase()
{
    // dao for this database
    abstract val dao : DatabaseDao


    // companion object
    companion object
    {
        @Volatile
        var databaseInstance : CardDatabase? = null


        // get database
        @InternalCoroutinesApi
        fun getInstance(context : Context) : CardDatabase
        {
            synchronized(this)
            {
                var instance = databaseInstance

                if (instance ==  null)
                {
                    instance =
                        Room
                            .databaseBuilder(context.applicationContext,
                                CardDatabase::class.java,
                                "card_database")
                            .fallbackToDestructiveMigration()
                            .build()

                    databaseInstance = instance
                }

                // log
                Log.i("getInstance Database", "database instance -> $databaseInstance")

                // return database
                return instance
            }
        }
    }
}