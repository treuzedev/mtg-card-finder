package eduardoaemunoz.mtgcardfinder.database


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface DatabaseDao
{
    // add card to database
    @Insert
    fun addCard(entity : CardEntity)

    // delete card from database
    @Query("delete from userCardsTable where id = :cardId")
    fun deleteCard(cardId : Int)

    // get all cards
    @Query("select * from userCardsTable")
    fun getAllCards() : List<CardEntity>

    // delete all cards
    @Query("delete from userCardsTable")
    fun deleteAllCards()
}