package eduardoaemunoz.mtgcardfinder.worker


import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import eduardoaemunoz.mtgcardfinder.database.CardDatabase
import kotlinx.coroutines.*


class CardWorker (appContext : Context,
                  parameters : WorkerParameters)
    : CoroutineWorker(appContext,
                      parameters)
{
    // companion object
    companion object
    {
        // properties
        const val PACKAGE = "eduardoaemunoz.mtgcardfinder"
        val uiScope = CoroutineScope(Dispatchers.Main)


        // clear cached images
        @InternalCoroutinesApi
        fun clearGlideCache(context : Context)
        {
            // variables
            val glide = Glide.get(context)
            val ioScope = CoroutineScope(Dispatchers.IO)
            val mainScope = CoroutineScope(Dispatchers.Main)

            // different functions, different threads
            mainScope.launch { glide.clearMemory() }
            ioScope.launch { glide.clearDiskCache() }
            loadImages1(context)

            // log
            Log.i("clearGlideCache", "clearGlideCache called with success!")
        }


        // background work
        @InternalCoroutinesApi
        fun loadImages1(context : Context)
        {
            uiScope.launch { loadImages2(context) }
        }

        @InternalCoroutinesApi
        suspend fun loadImages2(context : Context)
        {
            withContext(Dispatchers.IO)
            {
                loadImagesInBackground(context)
            }
        }

        @InternalCoroutinesApi
        fun loadImagesInBackground(context : Context)
        {
            // get dao database instance
            val database =
                CardDatabase
                    .getInstance(context)
                    .dao


            // val to hold card entities
            val cardList = database.getAllCards()

            // loop to load cards
            for (index in cardList.indices)
            {
                Glide
                    .with(context)
                    .setDefaultRequestOptions(RequestOptions()
                        .timeout(60000))
                    .load(cardList[index].url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .submit()
            }

            // log
            Log.i("loadImagesInBackground", "loadImagesInBackground called with success!")
        }
    }


    // do scheduled work
    @InternalCoroutinesApi
    override suspend fun doWork(): Result
    {
        // log
        Log.i("doWork", "doWork called!")


        // retrieve cards that user saved
        // load them into cache
        try
        {
            clearGlideCache(applicationContext)
            loadImages1(applicationContext)
        }

        // catch loading errors
        catch (error : Throwable)
        {
            Log.i("catch doWork fail", "loading failed -> ${error.message}")
            return Result.retry()
        }


        // log and return success
        Log.i("doWork success", "database images submitted to cache")
        return Result.success()
    }
}