package eduardoaemunoz.mtgcardfinder.fragments.list.recycler


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.databinding.CardListViewBinding
import eduardoaemunoz.mtgcardfinder.retrofit.Card


class ListAdapter : ListAdapter<Card, ListViewHolder>(MapDiffCallback())
{
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int) : ListViewHolder
    {
        // layout inflater
        val layoutInflater =
            LayoutInflater
                .from(parent.context)


        // binding object
        val binding : CardListViewBinding =
            DataBindingUtil
                .inflate(layoutInflater,
                    R.layout.card_list_view,
                    parent,
                    false)


        // return view with binding object
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder,
                                  position: Int)
    {
        // get item to display
        val card = getItem(position)

        // do logic
        val clicker = ListClickListener(holder, card)
        clicker.loadImg()
    }
}