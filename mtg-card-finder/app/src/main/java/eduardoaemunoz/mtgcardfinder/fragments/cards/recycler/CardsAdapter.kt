package eduardoaemunoz.mtgcardfinder.fragments.cards.recycler


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.databinding.CardDeckViewBinding
import eduardoaemunoz.mtgcardfinder.retrofit.Card


class CardsAdapter : ListAdapter<Card, CardsViewHolder>(MapDiffCallback())
{
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int) : CardsViewHolder
    {
        // layout inflater
        val layoutInflater =
            LayoutInflater
                .from(parent.context)


        // binding object
        val binding : CardDeckViewBinding =
            DataBindingUtil
                .inflate(layoutInflater,
                    R.layout.card_deck_view,
                    parent,
                    false)


        // return view with binding
        return CardsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CardsViewHolder,
                                  position: Int)
    {
        // get item displayed
        val card = getItem(position)

        // do logic
        val clicker = CardsClickListener(holder, card)
        clicker.loadImg()
    }
}