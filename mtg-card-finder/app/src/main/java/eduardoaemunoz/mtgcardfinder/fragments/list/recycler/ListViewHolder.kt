package eduardoaemunoz.mtgcardfinder.fragments.list.recycler


import androidx.recyclerview.widget.RecyclerView
import eduardoaemunoz.mtgcardfinder.databinding.CardListViewBinding


class ListViewHolder(val binding : CardListViewBinding)
    : RecyclerView.ViewHolder(binding.root)
{
    val cardImg = binding.cardImg
    val cardTitle = binding.cardTitle
    val cardSubtitle = binding.cardSubtitle
}