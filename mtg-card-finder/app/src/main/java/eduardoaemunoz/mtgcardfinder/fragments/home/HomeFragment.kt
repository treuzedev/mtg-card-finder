package eduardoaemunoz.mtgcardfinder.fragments.home


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.databinding.FragmentHomeBinding


class HomeFragment : Fragment()
{
    // binding object
    lateinit var binding : FragmentHomeBinding


    // view model
    lateinit var viewModel : HomeViewModel


    // lifecycle fun
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // binding object
        binding = DataBindingUtil
            .inflate(inflater,
                     R.layout.fragment_home,
                     container,
                     false)


        // view model
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        viewModel.parentBinding = binding
        viewModel.parentContext = this.requireContext()
        viewModel.parentFragment = this
        binding.viewModel = viewModel


        // return fragment
        return binding.root
    }
}
