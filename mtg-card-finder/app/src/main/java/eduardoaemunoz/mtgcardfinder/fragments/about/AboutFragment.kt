package eduardoaemunoz.mtgcardfinder.fragments.about


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.databinding.FragmentAboutBinding


class AboutFragment : Fragment()
{
    // binding object
    lateinit var binding : FragmentAboutBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // binding object
        binding = DataBindingUtil
            .inflate(
                inflater,
                R.layout.fragment_about,
                container,
                false)


        // return view
        return binding.root
    }
}
