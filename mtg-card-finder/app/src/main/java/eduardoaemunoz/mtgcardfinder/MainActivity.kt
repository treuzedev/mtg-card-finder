package eduardoaemunoz.mtgcardfinder


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.work.*
import eduardoaemunoz.mtgcardfinder.databinding.ActivityMainBinding
import eduardoaemunoz.mtgcardfinder.worker.CardWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity()
{
    // application scope
    val applicationScope =
        CoroutineScope(Dispatchers.Default)


    // binding object
    lateinit var binding : ActivityMainBinding


    // shared view model
    lateinit var sharedViewModel : SharedViewModel


    // lifecycle fun
    @InternalCoroutinesApi
    override fun onCreate(savedInstanceState : Bundle?)
    {
        super.onCreate(savedInstanceState)


        // binding object
        binding =
            DataBindingUtil
                .setContentView(this, R.layout.activity_main)


        // hide action bar
        supportActionBar!!.hide()


        // define bottom navigation bar
        val navView = binding.bottomNavigation
        val navController = findNavController(R.id.navHostFragment)
        val appBarConfiguration =
            AppBarConfiguration(topLevelDestinationIds =
            setOf(R.id.homeFragment, R.id.cardsFragment))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        // shared view model
        sharedViewModel = ViewModelProvider(this).get(SharedViewModel::class.java)


        // work manager
        backgroundWork()
    }


    // work manager request
    fun setupRecurringWork()
    {
        // set constraints
        val constraints =
            Constraints.Builder()
                .setRequiredNetworkType(NetworkType.NOT_ROAMING)
                .setRequiresBatteryNotLow(true)
                .setRequiresStorageNotLow(true)
                .build()


        // create repeating request
        val repeatingRequest =
            PeriodicWorkRequestBuilder<CardWorker>(1, TimeUnit.DAYS)
                .setConstraints(constraints)
                .build()


        // add a repeating request to be completed
        WorkManager
            .getInstance()
            .enqueueUniquePeriodicWork(
                CardWorker.PACKAGE,
                ExistingPeriodicWorkPolicy.KEEP,
                repeatingRequest)


        // log
        Log.i("setupRecurringWork", "setupRecurringWork called with success!")
    }


    // request work manager in the background
    @InternalCoroutinesApi
    fun backgroundWork()
    {
        applicationScope.launch { setupRecurringWork() }
    }
}
